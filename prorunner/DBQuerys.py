from mysql.connector import MySQLConnection, Error
from py_msql_dbconfig import read_db_config
 
 
def insert_runner(fname, lname, pnumber, dob, gender, address, email, username):
    query = "INSERT INTO prorunner_runners(fname,lname,pnumber,dob,gender,address,email,username) " \
            "VALUES(%s,%s,%s,%s,%s,%s,%s,%s)"
    args = (fname, lname, pnumber, dob, gender, address, email, username)

    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor()
            cursor.execute(query, args)
     
            if cursor.lastrowid:
                print('last insert id', cursor.lastrowid)
            else:
                print('last insert id not found')
     
            conn.commit()
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')

def create_event(event_name):
    isduplicate = "SELECT * FROM prorunner_event WHERE Ename = %s"
    insertQuery = "INSERT INTO prorunner_event(Ename) " \
            "VALUES(%s)"
    args = (event_name,)

    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(isduplicate, args)
            if cursor.rowcount == 0:
                cursor = conn.cursor()
                cursor.execute(insertQuery, args)
         
                if cursor.lastrowid:
                    print('last insert id', cursor.lastrowid)
                else:
                    print('last insert id not found')
         
                conn.commit()
            else:
                print('event already exists')

        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')

def fetch_events():
    fetch = "SELECT * FROM prorunner_event"

    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(fetch)
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')
        return cursor;

def fetch_chips():
    fetch = "SELECT * FROM prorunner_chip"

    db_config = read_db_config()

    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(fetch)
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')
        return cursor;


def fetch_races(eid):
    fetch = "SELECT * FROM prorunner_race WHERE eventid_id= %s"
    args = (eid,)
    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(fetch, args)
            print (cursor.rowcount)
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')
        return cursor;

def create_race(eid, rname, dist, stime):
    isduplicate = "SELECT * FROM prorunner_race WHERE Rname = %s"
    insertQuery = "INSERT INTO prorunner_race(eventid_id, rname, dist, start_timee) " \
            "VALUES(%s,%s,%s,%s)"
    dargs = (rname,)
    args = (eid, rname, dist, stime)
    print(args)

    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(isduplicate, dargs)
            if cursor.rowcount == 0:
                cursor = conn.cursor()
                cursor.execute(insertQuery, args)
         
                if cursor.lastrowid:
                    print('last insert id', cursor.lastrowid)
                else:
                    print('last insert id not found')
         
                conn.commit()
            else:
                print('event already exists')

        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')

def initial_signup(uid, rid, tshirt):
    isduplicate = "SELECT * FROM prorunner_signup WHERE runnerid_id = %s AND raceid_id = %s"
    insertQuery = "INSERT INTO prorunner_signup(runnerid_id, raceid_id, tshirt, bibno) " \
            "VALUES(%s,%s,%s,%s)"
    dargs = (uid, rid )
    args = (uid, rid, tshirt, None)

    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():

            cursor = conn.cursor(buffered=True)
            cursor.execute(isduplicate, dargs)

            if cursor.rowcount == 0:
                cursor = conn.cursor()
                cursor.execute(insertQuery, args)
         
                if cursor.lastrowid:
                    print('last insert id', cursor.lastrowid)
                else:
                    print('last insert id not found')
         
                conn.commit()
            else:
                print('event already exists')

        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')

def fetch_runnerid(username):
    fetch = "SELECT id FROM prorunner_runners WHERE username= %s"
    args = (username,)
    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(fetch, args)
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')
        row = cursor.fetchone()
        return row[0];

def fetch_signups(rid):
    fetch = "SELECT * FROM prorunner_signup s INNER JOIN prorunner_race r ON s.raceid_id = r.id INNER JOIN prorunner_event e ON r.eventid_id = e.id WHERE runnerid_id = %s ORDER BY s.id"
    args = (rid,)
    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(fetch, args)
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')
        return cursor;

def delete_signup(sid):
    delete = "DELETE FROM prorunner_signup WHERE id=%s"
    args = (sid,)
    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(delete, args)
            conn.commit()
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')

def update_bib(uid,rid,bib):
    update = "UPDATE prorunner_signup SET bibno=%s WHERE runnerid_id=%s AND raceid_id = %s"
    args = (bib,uid,rid)
    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(update, args)
            conn.commit()
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')

if __name__ == '__main__':
    connect()

def query_noCom(query, args):
    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(query, args)
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')
        return cursor;

def query_Com(query, args):
    db_config = read_db_config()
 
    try:
        print('Connecting to MySQL database...')
        conn = MySQLConnection(**db_config)
 
        if conn.is_connected():
            cursor = conn.cursor(buffered=True)
            cursor.execute(query, args)
            conn.commit()
        else:
            print('connection failed.')
 
    except Error as error:
        print(error)
 
    finally:
        conn.close()
        print('Connection closed.')