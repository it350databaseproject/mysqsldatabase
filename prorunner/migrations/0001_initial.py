# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Chip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('chipname', models.CharField(max_length=100)),
                ('descrip', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='chipAssign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bibno', models.IntegerField(default=None, null=True)),
                ('chipid', models.ForeignKey(to='prorunner.Chip')),
            ],
        ),
        migrations.CreateModel(
            name='chipReads',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField()),
                ('chipname', models.ForeignKey(to='prorunner.Chip')),
            ],
        ),
        migrations.CreateModel(
            name='Division',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gender', models.CharField(max_length=100)),
                ('beginAge', models.IntegerField()),
                ('endAge', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ename', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Persons',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fname', models.CharField(max_length=200)),
                ('lname', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Race',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rname', models.CharField(max_length=100)),
                ('dist', models.CharField(max_length=100)),
                ('start_timee', models.CharField(max_length=100)),
                ('eventid', models.ForeignKey(to='prorunner.Event')),
            ],
        ),
        migrations.CreateModel(
            name='Results',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('overalTime', models.DateTimeField()),
                ('age', models.IntegerField()),
                ('raceid', models.ForeignKey(to='prorunner.Race')),
            ],
        ),
        migrations.CreateModel(
            name='Runners',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fname', models.CharField(max_length=100)),
                ('lname', models.CharField(max_length=100)),
                ('pnumber', models.IntegerField()),
                ('dob', models.DateTimeField()),
                ('gender', models.CharField(max_length=100)),
                ('address', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('username', models.CharField(max_length=22)),
            ],
        ),
        migrations.CreateModel(
            name='Signup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tshirt', models.CharField(max_length=100)),
                ('bibno', models.IntegerField(default=None, null=True)),
                ('raceid', models.ForeignKey(to='prorunner.Race')),
                ('runnerid', models.ForeignKey(to='prorunner.Runners')),
            ],
        ),
        migrations.CreateModel(
            name='Timingpoint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pointid', models.IntegerField()),
                ('descrip', models.CharField(max_length=20)),
            ],
        ),
        migrations.AddField(
            model_name='results',
            name='runnerid',
            field=models.ForeignKey(to='prorunner.Runners'),
        ),
        migrations.AddField(
            model_name='division',
            name='eventid',
            field=models.ForeignKey(to='prorunner.Event'),
        ),
        migrations.AddField(
            model_name='chipreads',
            name='pointid',
            field=models.ForeignKey(to='prorunner.Timingpoint'),
        ),
        migrations.AddField(
            model_name='chipreads',
            name='rname',
            field=models.ForeignKey(to='prorunner.Race'),
        ),
        migrations.AddField(
            model_name='chipassign',
            name='raceid',
            field=models.ForeignKey(to='prorunner.Race'),
        ),
    ]
