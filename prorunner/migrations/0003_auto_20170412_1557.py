# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-12 15:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('prorunner', '0002_auto_20170412_0026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='results',
            name='runnerid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='relrunner', to='prorunner.Runners'),
        ),
    ]
