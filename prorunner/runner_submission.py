import mysql.connector
from mysql.connector import Error


def connect():
	"""Establish Connection"""
	try:
		conn = mysql.connector.connect(host='localhost',database='',user='root',password='byubyu')
		if conn.is_connected():
			print('Connection Established')

	except Error as e:
		print(e)

	finally:
		conn.close()


if __name__ == '__main__':
	connect()