from __future__ import unicode_literals

from django.db import models

# Create your models here.
#EXAMPLE
class Persons(models.Model):
	fname = models.CharField(max_length=200)
	lname = models.CharField(max_length=200)


class Runners(models.Model):
	fname = models.CharField(max_length = 100)
	lname = models.CharField(max_length = 100)
	pnumber = models.IntegerField()
	dob = models.DateTimeField()
	gender = models.CharField(max_length = 100)
	address = models.CharField(max_length = 100)
	email = models.CharField(max_length = 100)
	username = models.CharField(max_length = 22)

	def __str__(self):
		return '%s' % (self.fname)

class Event(models.Model):
	ename = models.CharField(max_length=100)

	def __str__(self):
		return '%s' % (self.ename)

class Race(models.Model):
	eventid = models.ForeignKey(Event, on_delete=models.CASCADE)
	rname = models.CharField(max_length=100)
	dist = models.CharField(max_length=100)
	start_timee = models.CharField(max_length=100)

	def __str__(self):
		return '%s' % (self.rname)


class Signup(models.Model):
	runnerid = models.ForeignKey(Runners, on_delete=models.CASCADE)
	raceid = models.ForeignKey(Race, on_delete=models.CASCADE)
	tshirt = models.CharField(max_length=100)
	bibno = models.IntegerField(default=None, null=True)


class Chip(models.Model):
	chipname = models.CharField(max_length=100)
	descrip = models.CharField(max_length = 20)

	def __str__(self):
		return '%s' % (self.chipname)


class chipAssign(models.Model):
	chipid = models.ForeignKey(Chip, on_delete=models.CASCADE)
	raceid = models.ForeignKey(Race, on_delete=models.CASCADE)
	bibno = models.IntegerField(default=None, null=True)


class Timingpoint(models.Model):
	pointid = models.IntegerField()
	descrip = models.CharField(max_length=20)


class chipReads(models.Model):
	pointid = models.ForeignKey(Timingpoint, on_delete=models.CASCADE)
	chipname = models.ForeignKey(Chip, on_delete=models.CASCADE)
	timestamp = models.IntegerField()
	rname = models.ForeignKey(Race, on_delete=models.CASCADE)


class Results(models.Model):
	runnerid = models.ForeignKey(Runners, on_delete=models.CASCADE, related_name="relrunner")
	raceid = models.ForeignKey(Race, on_delete=models.CASCADE)
	overalTime = models.DateTimeField()
	age = models.IntegerField()


class Division(models.Model):
	eventid = models.ForeignKey(Event, on_delete=models.CASCADE)
	gender = models.CharField(max_length=100)
	beginAge = models.IntegerField()
	endAge = models.IntegerField()
