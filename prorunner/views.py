from django.shortcuts import render, redirect, get_object_or_404

from django.http import HttpResponseRedirect, HttpResponse


from DBQuerys import insert_runner, create_event, fetch_events, create_race, fetch_races, initial_signup, fetch_runnerid, fetch_signups, delete_signup, update_bib, fetch_chips
from DBQuerys import query_noCom, query_Com

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required, user_passes_test
#name the prorunner.models to mymod
import prorunner.models as mymod
from django import forms
#import your models
from .models import Persons
from django.core.management.base import BaseCommand, CommandError
from django import forms
from .models import Chip
from numpy import genfromtxt
import csv
import codecs
import sys
import re
import string
import io
# Create your views here.

def is_event_admin(user):
    return user.is_authenticated() and user.groups.filter(name='EventAdmin').exists()

def login_func(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(username=username, password=password)
	if user is not None:
		login(request, user)
		return redirect(request.POST['next'], 'home')
	else:
		return redirect('login')

def addbib (request):
	username = request.POST.get('POSTusername', False)
	rid = request.POST.get('POSTrid', False)
	bib = request.POST.get('POSTbib', False)
	chip = request.POST.get('POSTchip', False)
	uid = fetch_runnerid(username)
	arg1 = (chip,)
	qresult = query_noCom("SELECT id FROM prorunner_chip WHERE chipname = %s", arg1)
	chips = qresult.fetchone()
	chipid = chips[0]
	arg2 = (chipid,rid,bib)
	query_Com("INSERT INTO prorunner_chipassign (chipid_id, raceid_id, bibno) VALUES (%s, %s, %s)", arg2)
	update_bib(uid,rid,bib)
	return render(request, 'home.html')

def registration (request):
	fname= request.POST.get('POSTfname', False)
	lname= request.POST.get('POSTlname', False)
	pnumber= request.POST.get('POSTpnumber', False)
	dob =request.POST.get('POSTdob', False)
	gender = request.POST.get('POSTgender', False)
	address = request.POST.get('POSTaddress', False)
	email = request.POST.get('POSTemail', False)
	username = request.POST.get('POSTusername', False)
	password = request.POST.get('POSTpassword', False)
	if User.objects.filter(username=username).exists():
		return render(request, 'denied.html')
	else:
		user = User.objects.create_user(username, email, password)
		user.first_name = fname
		user.last_name = lname
		user.groups.add(Group.objects.get(name='Runners'))
		user.save()
   		insert_runner(fname, lname, pnumber, dob, gender, address, email, username)
   		user = authenticate(username=username, password=password)
   		login(request, user)
   		return redirect('home')

def make_event(request):
	event_name = request.POST.get('POSTEname', False)
	create_event(event_name)
	return redirect('events')

def make_race(request):
	eid = request.POST.get('POSTeventId', False)
	race_name = request.POST.get('POSTRname', False)
	distance = request.POST.get('POSTdist', False)
	startTime = request.POST.get('POSTStime', False)
	create_race(eid, race_name, distance, startTime)
	return redirect('races')

def enter_race(request):
	rid = request.POST.get('POSTrid', False)
	tshirt = request.POST.get('POSTtshirt', False)
	uid = fetch_runnerid(request.user.username)
	initial_signup(uid, rid, tshirt)
	return redirect('home')

def signup_p2(request):
	event=request.POST.get('POSTeid', False)
	eventsList=fetch_events()
	racesList=fetch_races(event)
	results = query_noCom("SELECT Ename FROM prorunner_event WHERE id = %s",(event,))
	context_dict = {}
	context_dict['sel_event'] = event
	context_dict['eventsList'] = eventsList.fetchall()
	context_dict['racesList'] = racesList.fetchall()
	context_dict['activeEvent'] = event
	context_dict['event_name'] = results.fetchone()
	return render (request, 'racesignup.html', context_dict)

def all_chips (request):
	#this looks to see if the form was submitted
	if request.method == "POST":
		form = addChip(request.POST)
		if form.is_valid():
			form.save()
			return redirect('chips.html')
	else:
		form = addChip()
	mychips = mymod.Chip.objects.all() #this displays all the objects in the "Chip" class
	n= mymod.Chip.objects.count()
	return render(request, 'chips.html', {'mychips': mychips, 'form': form, 'n': n})


def chip_reads (request):
	if request.method == "POST":
		eventResults = request.POST.get('drop1', False)
		print(eventResults)
		raceOptions = mymod.Race.objects.filter(eventid = eventResults)
		eventOptions = mymod.Event.objects.all()
		raceResults = request.POST.get('drop2', False)
		print("****THE RACE START*****")
		print(raceResults)
		formreads = chipreaders(request.POST)
		if raceResults > 0:
			print("****YOU NEED TO REDIRECT")
			print("This is the race results")
			print(raceResults)
			selectedRace = mymod.Race.objects.filter(pk=raceResults)
			selectedEvent = mymod.Event.objects.filter(pk=selectedRace.values('eventid'))
			print(selectedRace.values('eventid'))
			print(selectedEvent.values('ename'))
			return render(request, 'time_options.html', {'selectedEvent': selectedEvent, 'selectedRace': selectedRace})
	else:
		eventOptions = mymod.Event.objects.all()
		raceOptions = mymod.Race.objects.all()
		formreads = chipreaders(request.POST)
	return render(request, 'times.html', {'raceOptions': raceOptions, 'eventOptions': eventOptions},{'formreads': formreads})

def timeOptions (request, raceResults):
	print("****HERE IN TIME OPTIONS DEF Function")
 	return render(request, 'times_options.html')

def startTime (request, race):
	print(race)
	if request.POST and request.FILES:
		myfile = request.FILES['theFile'].read()
		myfile = myfile.strip('\r\n')
		print(myfile)
		currentline = myfile.split(",")
		inputs = []
		for line in currentline:
			inputs.append(line)
		print(inputs)
		readfile = csv.DictReader(myfile)
		print(readfile)
		chipread = mymod.chipReads.objects.all()
		timingpoint_instance = mymod.Timingpoint.objects.get(pointid = 1)
		race_instance = mymod.Race.objects.get(id = race)
		# chipname_instance = mymod.Chip.objects.get(chipname = 'kjer')
		# print(timingpoint_instance)
		# run = mymod.chipReads.objects.create(pointid=timingpoint_instance, chipname=chipname_instance, timestamp=0101010125, rname=race_instance)
		# 	inputs = []
		# for row in readfile:
			# print(row)
			# inputs.append(row)
			# print(inputs)
		# with open(myfile, 'w', 1) as f:
		# 	print(f)
			# thefile = myfile.read()
			# print(thefile)
		# for line in thefile:
		# 	line = str(line)
		# 	newText = line.replace(line[:4], "xxxx")
		# 	print(newText)
		# myfile.open()
		# textFile = myfile.read()
		# for line in textFile:
		# 	textFile = myfile.read()
		# 	line = str(line)
		# 	newText = line.replace(line[:4], "xxxx")
		# 	print(newText)
	return render(request, 'start.html', {'race': race, })

def finishTime (request, race):
	print(race)
	return render(request, 'finish.html')


def event_seelcted (request):
	return render(request, 'timers.html')

class chipreaders(forms.ModelForm):
	class Meta:
		model = mymod.chipReads
		fields = ('chipname', 'timestamp')


def delChip(request, chip):
	instance = get_object_or_404(mymod.Chip, id=chip)
	instance.delete()
	mychips = mymod.Chip.objects.all() #this displays all the objects in the "Chip" class
	return redirect('/chips/')

def editChip(request, chip):
	print("in EditCHIp*****")
	instance = get_object_or_404(mymod.Chip, id=chip)
	if request.method == "POST":

		print(instance)
		formedit = Chip_edit(request.POST, instance=instance)
		print(formedit)
		if formedit.is_valid():
	 		print("is valid!!!!:)")
	 		formedit.save()
	 		print(formedit)
	 		return redirect('/chips/')
	else:
		print("ELSE********")
		formedit = Chip_edit(instance = instance)
 #        form.save()
	return render(request, 'editChips.html', {'formedit': formedit, 'instance': instance})

class addChip(forms.ModelForm):
	class Meta:
		model = mymod.Chip
		fields = ('chipname', 'descrip')

class Chip_edit(forms.ModelForm):
	class Meta:
		model = mymod.Chip
		fields = ('chipname', 'descrip')

def results_events(request):
	events = mymod.Event.objects.all()
	return render(request, 'resultsAll.html', {'events': events})

def results_race(request, event):
	races = mymod.Race.objects.filter(eventid=event)
	eventselected = mymod.Event.objects.filter(id=event)
	return render(request, 'resultsRace.html', {'races': races, 'eventselected': eventselected})

def results_overall(request, race):
	raceselected = mymod.Race.objects.filter(id=race)
	raceresults = mymod.Results.objects.filter(raceid=race)
	# therunner = mymod.Results.objects.order_by('Runners__sortkey',).values('Runners_fname','Runners__lname')
	r = mymod.Race.objects.get(id=race)
	re = r.results_set.all()
	return render(request, 'resultsOverall.html', {'raceselected': raceselected, 'raceresults': raceresults, 're': re})

def onsite_p2(request):
	event=request.POST.get('POSTeid', False)
	eventsList=fetch_events()
	racesList=fetch_races(event)
	results = query_noCom("SELECT Ename FROM prorunner_event WHERE id = %s",(event,))
	context_dict = {}
	context_dict['eventsList'] = eventsList.fetchall()
	context_dict['racesList'] = racesList.fetchall()
	context_dict['activeEvent'] = event
	context_dict['event_name'] = results.fetchone()
	return render (request, 'onsite.html', context_dict)

def remove_from_profile(request):
	signup_id = request.POST.get('POSTid', False)
	print(signup_id)
	delete_signup(signup_id)
	return redirect('profile')

# class NameForm(forms.Form)
	# your_name = forms.CharField(label='Your Name', max_length=100)

# Grouped all the page renders

def person (request):
	return render(request, 'person.html')

@login_required
def signup (request):
	eventsList=fetch_events()
	context_dict = {}
	context_dict['eventsList'] = eventsList.fetchall()
	context_dict['phase2'] = False
	return render (request, 'racesignup.html', context_dict)

def form (request):
	return render(request, 'form.html')

def login_page(request):
	nexturl=request.GET.get('next')
	return render(request, 'login.html', {'nexturl' : nexturl})

def home(request):
	return render(request, 'home.html')

def logout_view(request):
	logout(request)
	return redirect('signup')

def access_denied(request):
	return render(request, 'denied.html')

@login_required
@user_passes_test(is_event_admin, login_url='denied')
def events (request):
	return render(request, 'eventcreate.html')

@login_required
@user_passes_test(is_event_admin, login_url='denied')
def races (request):
	eventsList=fetch_events()
	list = eventsList.fetchall()
	context_dict={}
	context_dict['eventsList'] = list
	context_dict['size'] = len(list)
	return render(request, 'racecreate.html', context_dict)

@login_required
def profile (request):
	uid = fetch_runnerid(request.user.username)
	signupList = fetch_signups(uid)
	context_dict={}
	list = signupList.fetchall()
	context_dict['registry'] = list
	context_dict['size'] = len(list)
	return render(request, 'profile.html', context_dict)

@login_required
@user_passes_test(is_event_admin, login_url='denied')
def onsite(request):
	eventsList=fetch_events()
	context_dict = {}
	context_dict['eventsList'] = eventsList.fetchall()
	context_dict['phase2'] = False
	return render (request, 'onsite.html', context_dict)