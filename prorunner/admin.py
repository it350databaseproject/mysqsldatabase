from django.contrib import admin
from .models import Persons, Runners, Event, Race, Signup, Chip, chipAssign, chipReads, Division, Results, Timingpoint

# from .models import Signup

# Register your models here.

admin.site.register(Persons),
admin.site.register(Runners),
admin.site.register(Event),
admin.site.register(Race),
admin.site.register(Signup),
admin.site.register(Chip),
admin.site.register(chipAssign),
admin.site.register(chipReads),
admin.site.register(Division),
admin.site.register(Results),
admin.site.register(Timingpoint)
# admin.site.register(Signup)


# Register your models here.
