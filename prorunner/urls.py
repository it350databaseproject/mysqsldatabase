from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
	url(r'^admin/', include(admin.site.urls)),
	#Page renders
	url(r'^person/', views.person, name="person"),
	url(r'^Register/', views.form, name="form"),
	url(r'^raceday/', views.onsite, name="onsite"),
	url(r'^admin/events/', views.events, name="events"),
	url(r'^admin/races/', views.races, name="races"),
	url(r'^signup/', views.signup, name="signup"),
	url(r'^resultsAll/', views.results_events, name="results_events"),
	url(r'^myProfile/', views.profile, name="profile"),
	url(r'^home/', views.home, name="home"),
	url(r'^accounts/login/', views.login_page, name="login"),
	url(r'^Access_Denied/', views.access_denied, name="denied"),
	url(r'^chips/', views.all_chips, name="all_chips"),
	url(r'^chipsdelete/chip=(?P<chip>\d*)$', views.delChip, name="delChip"),
	url(r'^chipsedit/chip=(?P<chip>\d*)$', views.editChip, name="editChip"),
	url(r'^editchips/', views.editChip, name="editChip"),
	url(r'^times/', views.chip_reads, name="chip_reads"),
	url(r'^timingStart/race=(?P<race>\d*)$', views.startTime, name="startTime"),
	url(r'^timingFinish/race=(?P<race>\d*)$', views.finishTime, name="finishTime"),
	url(r'^results/event=(?P<event>\d*)$', views.results_race, name="results_race"),
	url(r'^results/resultsOverall=(?P<race>\d*)$', views.results_overall, name="results_overall"),

	#function calls
	url(r'^logging/', views.login_func, name="loginpass"),
	url(r'^logout/', views.logout_view, name="logout"),
	url(r'^addbib/', views.addbib, name="bib_submit"),
	url(r'^create_event/', views.make_event, name="create_event"),
	url(r'^create_race/', views.make_race, name="create_race"),
	url(r'^registration/', views.registration, name="form_confirm"),
	url(r'^enter/', views.enter_race, name="enter_race"),
	url(r'^eselect/', views.signup_p2, name="signup_p2"),
	url(r'^deleteSignup/', views.remove_from_profile, name="deleteSignup"),
	url(r'^onsite/event/', views.onsite_p2, name="onsite_p2"),
]